import React from 'react';
import './input.styles.scss'

const Input = ({labelTitle, id, errors, touched, ...restProps}) => (
  <div className="form-group">
    <label
      htmlFor={id}
      className={errors ? 'is-invalid' : undefined}
    >
      {labelTitle}
    </label>
    <input
      id={id}
      value={restProps.value}
      name={restProps.name}
      autoComplete={restProps.autoComplete}
      {...restProps}
      className={errors ? 'is-invalid' : undefined}
    />
    {errors && (<div className="invalid-feedback">{errors}</div>)}
  </div>
);

Input.defaultProps = {
  value: '',
  name: '',
  autoComplete: 'off'
}

export default Input;
