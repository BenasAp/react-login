import React from 'react';
import { shallow } from '../../enzyme';
import Input from './input.component';

describe('<Input />', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Input/>);
  });

  it('Should render <Input/> with error message', () => {
    wrapper.setProps({errors: 'Error message'})
    expect(wrapper.find('.invalid-feedback')).toHaveLength(1)
  });

  it('Should render <Input/> with error class in label', () => {
    wrapper.setProps({errors: 'Error message'})
    expect(wrapper.find('label.is-invalid')).toHaveLength(1)
  });

  it('Should render <Input/> with error class in input', () => {
    wrapper.setProps({errors: 'Error message'})
    expect(wrapper.find('input.is-invalid')).toHaveLength(1)
  });
});
