import React from 'react';
import { shallow } from '../../enzyme';
import Button from './button.component';
import Loader from '../loader/loader.component';

describe('<Button />', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Button/>);
  });

  it('Should render <Button /> with text and loader if loading', () => {
    wrapper.setProps({isLoading: true})
    expect(wrapper.find(Loader)).toHaveLength(1);
  });

  it('Should render <Button /> without loader', () => {
    expect(wrapper.find(Loader)).toHaveLength(0);
  })
});
