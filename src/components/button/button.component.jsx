import React from 'react';
import Loader from '../loader/loader.component';
import './button.style.scss';

const Button = ({text, isLoading = false, classNames = '', ...props}) => (
  <button
    className={`btn ${classNames}`}
    disabled={isLoading}
    {...props}
  >
    {isLoading && (<Loader classNames='loader-sm'/>)}
    <span>{text}</span>
  </button>
);

export default Button;
