import React from 'react';
import { connect } from 'react-redux';
import { logout } from '../../redux';
import { Redirect } from 'react-router-dom';

import Button from '../button/button.component';
import './toolbar.style.scss'


const ToolBar = ({userState, logout}) => {

  const handleClick = () => {
    logout();
  }

  if (userState.token === null) {
    return <Redirect to={'/'}/>
  }
  return (
    <div className="ToolBar">
      <div className="container">
        <div className="toolbar-wrapper">
          <div className="brand">Login App</div>
          <Button text='Logout' onClick={handleClick} />
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    userState: state.user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(logout())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ToolBar);
