import React, { useState } from 'react';
import ValidateService  from '../../service/Validation/ValidateService';
import { connect } from 'react-redux';
import { login } from '../../redux';
import { Redirect } from 'react-router-dom';

import Input from '../../components/input/input.component';
import Button from '../../components/button/button.component';

import './login.style.scss';

export const Login = ({userState, login}) => {

  const [credentials, setCredentials] = useState({
    username: '',
    password: '',
  });
  const [errors, setErrors] = useState({});
  const [touched, setTouched] = useState({});

  const validate = {
    username: name => ValidateService.requireValidation(name),
    password: name => ValidateService.requireValidation(name),
  };


  const handleBlur = event => {
    const {name, value} = event.target;
    const {[name]: removedError, ...rest} = errors;

    const error = validate[name](value);
    setErrors({
      ...rest,
      ...(error && {[name]: touched[name] && error})
    });
  };

  const handleChange = event => {
    const {value, name} = event.target;

    setCredentials({
      ...credentials,
      [name]: value
    });

    setTouched({
      ...touched,
      [name]: true
    });
  };

  const handleSubmit = evt => {
    evt.preventDefault();

    const formValidation = Object.keys(credentials)
      .reduce((acc, key) => {
          const newError = validate[key](credentials[key]);
          const newTouched = {[key]: true};
          return {
            errors: {
              ...acc.errors,
              ...(newError && {[key]: newError})
            },
            touched: {
              ...acc.touched,
              ...newTouched
            }
          };
        },
        {
          errors: {...errors},
          touched: {...touched}
        }
      );
    setErrors(formValidation.errors);
    setTouched(formValidation.touched);

    if (
      !Object.values(formValidation.errors).length &&
      Object.values(formValidation.touched).length ===
      Object.values(credentials).length &&
      Object.values(formValidation.touched).every(t => t === true)
    ) {
      login(credentials);
    }
  };
  if (!userState.loading && userState.token !== null) {
    return <Redirect to={'/dashboard'}/>
  } else {
    return (
      <div className='Login'>
        <div className='container'>
          <div className="card">
            <form onSubmit={handleSubmit}>
              <div className="card-header">
                LOGIN IN
              </div>
              <div className="card-body">
                <Input
                  type="text"
                  name="username"
                  value={credentials.username}
                  id="username"
                  labelTitle="Name"
                  disabled={userState.loading}
                  placeholder="User name"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errors={errors.username && errors.username}
                />
                <Input
                  type="password"
                  name="password"
                  value={credentials.password}
                  id="password"
                  labelTitle="Password"
                  disabled={userState.loading}
                  placeholder="Password"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  errors={errors.password && errors.password}
                />
                <div className="invalid-feedback">{userState.errors}</div>

              </div>
              <div className="card-actions">
                <Button isLoading={userState.loading} type="submit" text="Login"/>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
};

const mapStateToProps = state => {
  return {
    userState: state.user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    login: (credentials) => dispatch(login(credentials))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
