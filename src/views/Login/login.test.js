import React from 'react';
import { shallow } from '../../enzyme';
import { Redirect } from 'react-router-dom';
import { Login } from './login.components';

describe('<Login />', () => {
  let wrapper;
  const state = {
    loading: false, token: 'token'
  };

  beforeEach(() => {
    wrapper = shallow(<Login userState={state}/>);
  });

  it('Should render <Login /> redirect after login', () => {
    expect(wrapper.find(Redirect)).toHaveLength(1)
  });
});
