import React, { useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { fetchServerList, sortByDistance, sortByName } from '../../redux';
import ToolBar from '../../components/toolbar/toolbar.component';
import Loader from '../../components/loader/loader.component';
import sortIcon from '../../assets/images/sort-down.svg'
import './dashboard.style.scss'

const Dashboard = ({fetchServerList, userState, serverList, sortByDistance, sortByName}) => {

  useEffect(() => {
    if (serverList.data === null && userState.isUserLoggedIn) {
      fetchServerList();
    }
  }, [serverList.data, fetchServerList, userState.isUserLoggedIn]);

  const sortByNameHandler = () => {
    sortByName(serverList.data);
  }
  const sortByDistanceHandler = () => {
    sortByDistance(serverList.data);
  }
  return userState.isUserLoggedIn ? (
    <div className="Dashboard">
      <ToolBar/>
      <div className="spacer"></div>
      <div className="container">
        <div className="card">
          <div className="card-body">
            {serverList.loading ? (
              <div className="centered"><Loader/></div>
            ) : (
              <table className="table">
                <thead>
                <tr className="sorted-tr">
                  <th onClick={sortByDistanceHandler}>
                    <div>
                      <span>Distance</span>
                      <img src={sortIcon} alt="sort-icon"/>
                    </div>
                  </th>
                  <th onClick={sortByNameHandler}>
                    <div>
                      <span>Name</span>
                      <img src={sortIcon} alt="sort-icon"/>
                    </div>
                  </th>
                </tr>
                </thead>
                <tbody>
                {
                  serverList &&
                  serverList.data &&
                  serverList.data.map((item, index) => (
                    <tr key={index}>
                      <td>{item.distance}</td>
                      <td>{item.name}</td>
                    </tr>
                  ))
                }
                </tbody>
              </table>
            )}
          </div>
        </div>
      </div>
      <div className="spacer"></div>
    </div>
  ) : (
    <Redirect to={'/'}/>
  );
};
const mapStateToProps = state => {
  return {
    userState: state.user,
    serverList: state.serverList
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchServerList: () => dispatch(fetchServerList()),
    sortByDistance: () => dispatch(sortByDistance()),
    sortByName: () => dispatch(sortByName())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
