import React, { useEffect } from 'react';
import { Route, Switch } from 'react-router-dom'
import './assets/sass/styles.scss'
import { loginUserRequest, loginUserSuccess } from './redux';
import { connect } from 'react-redux'

import Login from './views/Login/login.components';
import Dashboard from './views/Dashboard/dashboard.component';
import './App.scss';
import AuthService from "./service/Auth/AuthService";

function App({loginUserSuccess, loginUserRequest}) {

  useEffect(() => {
    const user = localStorage.getItem('user');
    if (user) {
      loginUserRequest();
      const userData = JSON.parse(user);
      AuthService.setBearer(userData.token);
      loginUserSuccess(userData);
    }
  }, [loginUserSuccess, loginUserRequest]);

  return (
    <Switch>
      <Route exact path='/' component={Login}/>
      <Route exact path='/dashboard' component={Dashboard}/>
    </Switch>
  );
}

const mapDispatchToProps = dispatch => {
  return {
    loginUserSuccess: user => dispatch(loginUserSuccess(user)),
    loginUserRequest: () => loginUserRequest()
  }
}

export default connect(null, mapDispatchToProps)(App);
