import { combineReducers } from 'redux';

import userReducer from './user/userReducer';
import serverListReducer from './serversList/serverListReducer';

export default combineReducers({
  user: userReducer,
  serverList: serverListReducer
});
