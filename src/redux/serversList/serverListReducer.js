import {
  FETCH_SERVER_LIST_REQUEST,
  FETCH_SERVER_LIST_SUCCESS,
  FETCH_SERVER_LIST_FAILURE,
  SORT_SERVER_LIST_BY_NAME,
  SORT_SERVER_LIST_BY_DISTANCE,
} from './serverListActionTypes';

import ServerListService from '../../service/ServerList/ServerListService';

const INITIAL_STATE = {
  loading: false,
  data: null,
  errors: '',
}

const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_SERVER_LIST_REQUEST:
      return {
        ...state,
        loading: true
      }

    case FETCH_SERVER_LIST_SUCCESS:
      return {
        loading: false,
        data: action.payload,
        errors: ''
      }

    case FETCH_SERVER_LIST_FAILURE:
      return {
        loading: false,
        data: null,
        errors: action.payload
      }

    case SORT_SERVER_LIST_BY_DISTANCE:
      return {
        ...state,
        data: ServerListService.sortByDistance(state.data)
      }

    case SORT_SERVER_LIST_BY_NAME:
      return {
        ...state,
        data: ServerListService.sortByName(state.data)
      }


    default:
      return state;
  }
}

export default userReducer;
