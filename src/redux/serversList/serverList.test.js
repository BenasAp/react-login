import serverList from './serverListReducer';

describe('serverList reducer', () => {
  const initState = {
    loading: false,
    data: null,
    errors: '',
  }

  it('Should return initial state', () => {
    expect(serverList(undefined, {})).toEqual(initState)
  });
});
