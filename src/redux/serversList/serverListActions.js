import {
  FETCH_SERVER_LIST_REQUEST,
  FETCH_SERVER_LIST_SUCCESS,
  FETCH_SERVER_LIST_FAILURE,
  SORT_SERVER_LIST_BY_NAME,
  SORT_SERVER_LIST_BY_DISTANCE
} from './serverListActionTypes';

import ServerListService from '../../service/ServerList/ServerListService';

export const fetchServerListRequest = () => {
  return {
    type: FETCH_SERVER_LIST_REQUEST
  }
}

export const fetchServerListSuccess = serverList => {
  return {
    type: FETCH_SERVER_LIST_SUCCESS,
    payload: serverList,
  }
}

export const fetchServerListFailure = error => {
  return {
    type: FETCH_SERVER_LIST_FAILURE,
    payload: error
  }
}

export const sortByName = () => {
  return {
    type: SORT_SERVER_LIST_BY_NAME,
  }
}

export const sortByDistance = () => {
  return {
    type: SORT_SERVER_LIST_BY_DISTANCE,
  }
}

export const fetchServerList = () => {
  return (dispatch) => {
    dispatch(fetchServerListRequest());
    ServerListService.fetchServerList()
      .then(res => {
        const serverList = res.data;
        dispatch(fetchServerListSuccess(serverList));
      })
      .catch(error => {
        const errorMsg = error.response.data.message;
        dispatch(fetchServerListFailure(errorMsg))
      })
  }
}
