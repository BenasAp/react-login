import { LOGIN_USER_REQUEST, LOGIN_USER_SUCCESS, LOGIN_USER_FAILURE, LOGOUT_USER } from './userActionTypes'
import AuthService from '../../service/Auth/AuthService';

export const loginUserRequest = () => {
  return {
    type: LOGIN_USER_REQUEST
  }
}

export const loginUserSuccess = user => {
  return {
    type: LOGIN_USER_SUCCESS,
    payload: user,
  }
}

export const loginUserFailure = error => {
  return {
    type: LOGIN_USER_FAILURE,
    payload: error
  }
}

export const login = (credentials) => {
  return (dispatch) => {
    dispatch(loginUserRequest());
    AuthService.loginUser(credentials)
      .then((res) => {
        const user = res.data;
        AuthService.setBearer(res.data.token);
        localStorage.setItem('user', JSON.stringify(res.data));
        dispatch(loginUserSuccess(user));
      })
      .catch(error => {
        const errorMsg = error.response.data.message;
        dispatch(loginUserFailure(errorMsg));
      })
  }
}

export const logout = () => {
  AuthService.logoutUser();
  AuthService.clearBearer();
  return {type: LOGOUT_USER}
}
