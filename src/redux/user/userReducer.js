import { LOGIN_USER_REQUEST, LOGIN_USER_SUCCESS, LOGIN_USER_FAILURE, LOGOUT_USER } from './userActionTypes'

const INITIAL_STATE = {
  loading: false,
  token: null,
  isUserLoggedIn: false,
  errors: '',
}

const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN_USER_REQUEST:
      return {
        ...state,
        loading: true
      }

    case LOGIN_USER_SUCCESS:
      return {
        loading: false,
        isUserLoggedIn: true,
        token: action.payload,
        errors: ''
      }

    case LOGIN_USER_FAILURE:
      return {
        loading: false,
        isUserLoggedIn: false,
        token: null,
        errors: action.payload
      }

    case LOGOUT_USER:
      return {
        loading: false,
        isUserLoggedIn: false,
        token: null,
        errors: '',
      }

    default:
      return state;
  }
}

export default userReducer;
