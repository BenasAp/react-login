import userReducer from './userReducer';

describe('User reducer', () => {
  const initState = {
    loading: false,
    token: null,
    isUserLoggedIn: false,
    errors: '',
  }

  it('Should return initial state', () => {
    expect(userReducer(undefined, {})).toEqual(initState)
  });
});
