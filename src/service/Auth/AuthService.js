import axios from 'axios';
import { URL } from '../../config/api';

export default {
  setBearer: token => {
    axios.defaults.headers.common = {Authorization: `Bearer ${token}`};
  },
  clearBearer: () => {
    axios.defaults.headers.common = {Authorization: null};
  },
  loginUser: credentials => axios.post(`${URL}/tokens`, credentials),
  logoutUser: () => {
    localStorage.removeItem('user')
  }
};
