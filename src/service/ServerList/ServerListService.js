import axios from 'axios';

import { URL } from '../../config/api';

export default {
  fetchServerList: () =>
    axios.get(`${URL}/servers`),
  sortByName: data => data.sort((a, b) => a.name < b.name ? -1 : (a.name > b.name ? 1 : 0)),
  sortByDistance: data => data.sort((a, b) => a.distance - b.distance)
};
