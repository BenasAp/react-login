import ServerListService from './ServerListService';

describe('ServerListService', () => {
  const nameArray = [{name: 'd'}, {name: 'b'}, {name: 'g'}, {name: 'c'}, {name: 'a'}];
  const numberArray = [{distance: '43'}, {distance: '567'}, {distance: '1'}, {distance: '876'}, {distance: '32'}];

  it('Should render return sorted array by name', () => {
    expect(ServerListService.sortByName(nameArray)).toEqual(
      [{name: 'a'}, {name: 'b'}, {name: 'c'}, {name: 'd'}, {name: 'g'}]
    );
  });

  it('Should render return sorted array by numbers', () => {
    expect(ServerListService.sortByDistance(numberArray)).toEqual(
      [{distance: '1'}, {distance: '32'}, {distance: '43'}, {distance: '567'}, {distance: '876'}]
    );
  });
});
