export default {
  requireValidation: fieldValue => {
    if (fieldValue.trim() === '') {
      return 'This field is required';
    }

    return null;
  },
}
